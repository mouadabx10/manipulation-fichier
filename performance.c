#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define N_MAX 126000000


////////////////// PARTIE : CRÉATION DE LA STRUCTURE DE DONNÉES : DATA ////////////////////////

struct data {
        int numero;
        char *rue;
        char *ville;
        int code_postal;
};
typedef struct data data;

/*
Retourne l'attribut de la structure data selon le critère donné en caractère
*/
int criteria_data(data my_data, char *criteria){
        if(strcmp(criteria, "numero") == 0){ // criteria = "numero"
                return my_data.numero;
        }else if(strcmp(criteria, "code postal") == 0){ // criteria = "code postal"
                return my_data.code_postal;
        }else{ // criteria different
                return -1;
        }
}

/*
Fonction pour afficher les attributs de data selon le critère donné en paramètre (Fonction utile pour la partie tests)
*/
void afficher_data(data my_data, char* criteria){
        printf("data : %d -> ", criteria_data(my_data, criteria));
}

/*
Fonction qui sépare une chaine de caractère en sous-chaines de caractères selon un séparateur (dans notre cas, il s'agit de la virgule)
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////::: ajouter ici §§§§§§§§§§§§§§ ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////:
//char *strsep(char *s){}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////::: ajouter ici §§§§§§§§§§§§§§ ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////:

/*
Fonction qui crée une instance data selon une chaine de caractères donnée en paramètre
my_string est organisé comme suit : XXXXXX, XXXXXX, numero, rue, XXXXXX, ville, XXXXXX, XXXXXX, code_postal
*/
data creer_data(char *my_string){
        data my_data; // Créer une instance vide de data
        char *s = strdup(my_string); // Récuperer la ligne my_string
        int i=1;
        char *val = strsep(&s, ","); // Séparer la chaine de caractère 
        while(val != NULL){ // NULL définit qu'on est arrivé à la fin de la ligne
                if(i == 3){ // On prend la troisième chaine de caractère
                        my_data.numero = atoi(val); // atoi permet de convertir char* en int
                }
                else if(i == 4){ // On prend la quatrième chaine de caractère
                        my_data.rue = val;
                }
                else if(i == 6){ // On prend la sixième chaine de caractère
                        my_data.ville = val;
                }
                else if(i == 9){ // On prend la neuvième chaine de caractère
                        my_data.code_postal = atoi(val);
                }
                else{} //Les autres colonnes de la ligne ne nous intéressent pas
                i++;
                val = strsep(&s, ","); // On passe à la chaine de caractère suivante
        }
        return my_data; // Retourne l'instance de data rempli avec les bons attributs
}

////////////////////////// PARTIE : TRI BRETON /////////////////////////////////////

/*
Fonction qui inverse une partie du tableau tab[start...end]
ex: tab = [2, 7, 6, 1, 0, 8, 9]
    inverse_tab(tab, 3, 6) = [2, 7, 6, 9, 8, 0, 1]
*/
void inverse_tab(data *tab, int start, int end){
        int incr = 0;
        int milieu = (end - start) / 2; // Donne l'indice milieu de la partie du tableau à inverser
        if((end - start) % 2 == 1){
                milieu = 1 + milieu; // Si la longueur du tableau est impaire, on ajoute 1 à l'indice milieu
        }
        while (incr < milieu){ // Tant qu'on arrive pas au milieu, on inverse les deux éléments du tableau symétriques par rapport à l'indice milieu
                data tmp = tab[start + incr]; 
                tab[start + incr] = tab[end - incr];
                tab[end - incr] = tmp;
                incr ++;
        }
}

/*
Retourne l'indice de l'élément maximal de la partie du tableau tab[start_tab, index]
ex : tab = [2, 7, 6, 1, 10, 8, 4]
     max_tab(tab, 2, 6) = 4 (10 est le max des nombres 6, 1, 10, 8, 4)
     criteria : critère par rapport auquel on définit le max
*/
int max_tab(data *tab, int start_tab, int index, char *criteria){
        if(index == start_tab) {return start_tab;}
        int max = start_tab;
        for(int i=start_tab + 1; i<=index; i++){
                if(criteria_data(tab[i], criteria) > criteria_data(tab[max], criteria)){
                        max = i;
                }
        }
        return max;
}


/*
Fonction qui tri le tableau selon le critère donné en paramètre
*/
void tri_breton(data *tab, char *criteria, int len){
        int max;
        for (int i=0; i<=len-1; i++) {
                int max = max_tab(tab, i, len-1, criteria);
                inverse_tab(tab, max, len-1);
                //Inverser toute la pile
                inverse_tab(tab, i, len-1);
        }
        inverse_tab(tab, 0, len-1);
}

/*
 * Fonction implémentant le tri par insertion selon le critère donné en paramètre
 */
void tri_insertion(data *tab, char *criteria, int len){
        data tmp;
        int j;
        for(int i=1; i<=len-1; i++){
                j=i;
                while((criteria_data(tab[j-1], criteria) > (criteria_data(tab[j], criteria)))&&(j>0)){
                        tmp = tab[j];
                        tab[j] = tab[j-1];
                        tab[j-1] = tmp;
                        j--;
                }
        }
}

void tri_selection(data *tab, char *criteria, int len){
        data tmp;
        for (int i=0; i<len-1; i++){
                for(int j=i+1; j<len; j++){
                        if(criteria_data(tab[i], criteria)>criteria_data(tab[j], criteria)){
                                tmp = tab[i];
                                tab[i] = tab[j];
                                tab[j] = tmp;
                        }
                }
        }
}
//////////////////////////////// PARTIE : CALCUL DES PERFORMANCES //////////////////////////////

/* Variable globale pour stocker les data du fichier france.csv */
data *tableau_data;


/*
Mesure le temps mis pour extraire les données du fichier en fonction du nombre de lignes extraites
*/
void mesure_performance(){
        char message[100];
        int i=0;
        FILE *out = fopen("performance.csv", "a");//Fichier pour stocker les performances d'extraction de données (ouvert en écriture)

        FILE *f = fopen("france.csv", "r"); //Fichier pour extraire les données (ouvert en lecture)
        clock_t debut = clock();
        int pas = 100000; // Pas de la boucle while (on commence par extraire 10^5 puis 2.10^5 ...)
        while (fgets(message, 100, f)) {
                if(i != 0){// On ne prend pas en compte la première ligne car elle comprend les titres des colonnes 
                        data my_data = creer_data(message); // On crée l'instance data correspondant à la ligne 
                        tableau_data[i-1] = my_data; // On stocke l'instance data créée dans la variable global tableau_data
                        if(i == pas || i == N_MAX-1){ // On s'arrete si on arrive à la ligne max
                                clock_t fin = clock();
                                fprintf(out, "%d , %d \n", pas, (1000*(fin - debut)/CLOCKS_PER_SEC)); //On écrit le temps de performance dans le fichier ouvert en ecriture
                                pas += 100000; //On incremente le nombre de lignes extraites
                        }
                }
                i++;
        }
        fclose(f);
        fclose(out);
}

/*
Mesure le temps mis pour trier les données de la variable globale tableau_data
*/
void tri_performance(){
        FILE *tri = fopen("tri.csv", "a"); //Fichier pour stocker les performances de tri (ouvert en écriture)
        for(int i=100; i<20000; i+=100){
                clock_t debut = clock();
                tri_breton(tableau_data, "numero", i); //Algorithme du tri breton selon criteria = numero
                clock_t fin = clock();
                fprintf(tri, "%d , %d \n", i, (1000*(fin - debut)/CLOCKS_PER_SEC)); // Ecrit la mesure de performance dans le fichier tri.csv
        }
        fclose(tri);
}

void tri_performance_selection(){
        FILE *tri = fopen("tri_insertion.csv", "a"); //Fichier pour stocker les performances de tri (ouvert en écriture)
        for(int i=100; i<20000; i+=100){
                clock_t debut = clock();
                tri_selection(tableau_data, "numero", i); //Algorithme du tri breton selon criteria = numero
                clock_t fin = clock();
                fprintf(tri, "%d , %d \n", i, (1000*(fin - debut)/CLOCKS_PER_SEC)); // Ecrit la mesure de performance dans le fichier tri.csv
        }
        fclose(tri);
}

int main (void) {
        tableau_data = malloc(N_MAX * sizeof(data)); // Allouer l'espace memoire du tableau_data
        printf("Début performance \n");
        mesure_performance(); // La mesure de performance de lecture du fichier france.csv et remplissage de performance.csv
        printf("Fin performance \nDébut Tri \n");
        //tri_performance(); // ---------------- TRI BRETON
        //tri_performance_selection(); // -------------------- TRI SELECTION
        printf("Fin\n");

}
